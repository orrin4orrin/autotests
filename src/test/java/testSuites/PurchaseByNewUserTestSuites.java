package testSuites;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.CheckoutPage;
import pages.Header;
import properties.User;
import services.CustomService;
import services.Log;
import services.PaymentMethod;

public class PurchaseByNewUserTestSuites extends BaseTest {
    @Test
    public void purchaseByNewUserTest() {
        CustomService service = new CustomService(driver);
        User user = new User();
        Log.info("Navigate to preview page");
        driver.get("https://www.templatemonster.com/drupal-themes/50509.html");
        Header headerPage = PageFactory.initElements(driver, Header.class);
        headerPage.waitForHeartEnable();

        CartPage cartPage = PageFactory.initElements(driver, CartPage.class);
        cartPage.clickOnAddToCart();
        cartPage.selectRandomOffer("OnTemplate");
        cartPage.selectRandomOffer("OnCart");
        cartPage.clickOnCheckoutButton();

        CheckoutPage checkoutPage = PageFactory.initElements(driver, CheckoutPage.class);
        service.waitUrl("checkout");
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.templatemonster.com/checkout.php", "Incorrect URL");
        checkoutPage.registerNewUser(user);
        Cookie cookieLgn = checkoutPage.getCookieLgn();
        Assert.assertEquals(service.urlDecode(cookieLgn.getValue()), user.getEmail());

        checkoutPage.payByPaymentMethod(PaymentMethod.Skrill);
        service.switchToLastWindow();
        service.waitUrl("pay");
    }
}

