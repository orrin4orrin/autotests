package services;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertTrue;

public class CustomService {
    private WebDriver driver;

    public CustomService(WebDriver driver) {
        this.driver = driver;
    }

    Random random = new Random();

    public void waitClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitUrl(String url) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.urlContains(url));
    }

    public void waitForElementVisible(WebElement element) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (TimeoutException e) {
            assertTrue(false, "Element" + element + "is not found");
        }
    }

    public void clickOnElement(WebElement element, String elementName) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException ex) {
            return;
        }
        try {
            element.click();
        } catch (NoSuchElementException e) {
            assertTrue(false, "\"" + elementName + "\" not found on page after timeout");
        } catch (StaleElementReferenceException e) {
            element.click();
        }
    }

    public void chooseRandomElementInList(List<WebElement> list) {
        if (list.isEmpty()) {
            Log.warn("Template hasn't lists");
            return;
        }
        int numberOnTemplate = random.nextInt(list.size());
        this.waitClickable(list.get(numberOnTemplate));
        this.clickOnElement(list.get(numberOnTemplate), "Random element");
        Log.info("Random offer added");
        this.waitClickable(list.get(numberOnTemplate));
    }

    public void switchToLastWindow() {
        driver.getWindowHandles().forEach(driver.switchTo()::window);
    }

    public void goTo(String url) {
        driver.get(url);
    }

    public static void waitForCookie(final String cookieName, WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(new ExpectedCondition<Boolean>() {
                @Override
                public Boolean apply(WebDriver driver) {
                    return (driver.manage().getCookieNamed(cookieName) != null);
                }
            });
        } catch (TimeoutException e) {
            Assert.assertFalse(true, "failed");
        }
    }

    public String urlDecode(String url) {
        try {
            return URLDecoder.decode(url, "UTF-8");
        } catch (UnsupportedEncodingException var2) {
            assertTrue(Boolean.valueOf(false), var2.toString());
            return null;
        }
    }
}


