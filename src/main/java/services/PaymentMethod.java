package services;

public enum  PaymentMethod {
    PayPal,
    Stripe,
    Skrill,
    PayProGlobal
}
