package properties;

import lombok.Data;

@Data
public class User {
    private String email = "orrin-"+System.currentTimeMillis()+"@templatemonster.me";
    private String name = "orrin test";
    private String countryCode = "UA";
    private String phone = "123123123";
    private String zip = "7777";
    private String city = "Nick";
}
