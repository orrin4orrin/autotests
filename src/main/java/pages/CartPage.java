package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.CustomService;
import services.Log;

import java.util.List;

public class CartPage {
    private final WebDriver driver;
    private CustomService service;

    public CartPage(WebDriver driver){
        this.driver=driver;
        service = new CustomService(driver);
    }

    @FindBy(xpath = ".//*[contains(@class, 'add-to-cart')]//*[contains(@class, 'js-button-add-to-cart')]")
    private WebElement addToCartButton;

//    @FindBy(id = "cart-summary-checkout")
//    private WebElement checkoutButton;

    @FindBy(xpath = ".//button[contains(@class, 'js-checkout-button')]")
    private WebElement checkoutButton;

    @FindBy(xpath = ".//*[contains(@class, 'js-total-price')]")
    private WebElement orderTotal;

    @FindBy(xpath = ".//*[@class='template-shopping-options recommended']/ul/li[1]//*[contains(@class, 'js-add-offer')]")
    private WebElement offerOnTemplate;

    @FindBy(xpath = ".//*[@class='template-shopping-options upsells']/ul/li[1]//*[contains(@class, 'js-add-offer')]")
    private WebElement offerOnCart;

    @FindBy(xpath = ".//*[@class='template-shopping-options upsells']//ul//li//*[contains(@id, 'sc-add-offer-oncart')]")
    public List<WebElement> offerOnTemplateList;

    @FindBy(xpath = ".//*[@class='template-shopping-options recommended']//ul//li//*[contains(@id, 'sc-add-offer')]")
    public List<WebElement> offerOnCartList;

    @FindBy (xpath = ".//*[contains(@class, 'js-popup-cart-item offer')]//*[contains(@class, 'js-txt-price')]")
    private WebElement onTemplateOfferAddedInCart;

    @FindBy (xpath = ".//*[contains(@class, 'js-parent-oncart')]//*[contains(@class, 'price js-price')]")
    private WebElement onCartOfferAddedInCart;

    public void clickOnAddToCart() {
        service.waitClickable(addToCartButton);
        addToCartButton.click();
        Log.info("Template added to cart");
    }

    public void selectRandomOffer(String offerType){
        service.waitClickable(checkoutButton);
        service.waitForElementVisible(orderTotal);
        switch (offerType){
            case "OnTemplate":
                service.chooseRandomElementInList(offerOnTemplateList);
                break;
            case "OnCart":
                service.chooseRandomElementInList(offerOnCartList);
                break;
            default:
                throw new IllegalArgumentException("Incorrect offer name, choose OnCart or OnTemplate offer name");
        }
    }

    public void clickOnCheckoutButton() {
        service.waitForElementVisible(orderTotal);
        service.waitForElementVisible(onTemplateOfferAddedInCart);
        service.waitForElementVisible(onCartOfferAddedInCart);
        service.waitClickable(checkoutButton);
        checkoutButton.click();
        Log.info("Navigate to checkout page");
    }
}
