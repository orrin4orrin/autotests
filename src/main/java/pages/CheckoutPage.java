package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import properties.User;
import services.CustomService;
import services.Log;
import services.PaymentMethod;

import java.util.List;

public class CheckoutPage {
    private final WebDriver driver;
    private CustomService service;

    public CheckoutPage(WebDriver driver) {
        this.driver = driver;
        service = new CustomService(driver);
    }

    @FindBy(id = "signin3-form-email")
    private WebElement emailField;

    @FindBy(id = "signin3-new-customer")
    WebElement continueButton;

    @FindBy(id = "billinginfo3-form-fullname")
    private WebElement fullName;

    @FindBy(id = "billinginfo3-form-phone")
    private WebElement phoneNumber;

    @FindBy(id = "billinginfo3-form-postalcode")
    private WebElement zipCode;

    @FindBy(xpath = ".//*[contains(@id,'form-cityname')]")
    private WebElement cityName;

    @FindBy(id = "billinginfo3-form-submit")
    WebElement submitBillingDetailsButton;

    @FindBy(xpath = "//fieldset//div[4]//*[contains(@class, 'rd-field-success')]//span")
    private WebElement activeIconPostalCode;

    @FindBy(xpath = "//*[contains(@id,'checkout-payment-buy')]")
    public List<WebElement> paymentList;

    public void enterEmail(String email){
        emailField.sendKeys(email);
    }

    public void enterFullName(String name){
        fullName.sendKeys(name);
    }

    public void enterPhone(String phone){
        phoneNumber.sendKeys(phone);
    }

    public void enterZipCode(String zip){
        zipCode.sendKeys(zip);
    }

    public void enterCityName(String city){
        cityName.sendKeys(city);
    }

    public void registerNewUser(User user){
        service.waitClickable(emailField);
        enterEmail(user.getEmail());
        continueButton.click();
        enterFullName(user.getName());
        enterPhone(user.getPhone());
        enterZipCode(user.getZip());
        service.waitForElementVisible(activeIconPostalCode);
        enterCityName(user.getCity());
        service.waitForElementVisible(submitBillingDetailsButton);
        submitBillingDetailsButton.click();
        Log.info("New user registered");
    }

    public Cookie getCookieLgn() {
        service.waitForCookie("lgn", driver);
        return driver.manage().getCookieNamed("lgn");
    }

    public void payByPaymentMethod(PaymentMethod paymentMethod){
        service.waitClickable(paymentList.get(1));
        WebElement payment = driver.findElement(By.id("checkout-payment-buy-" + paymentMethod));
        service.clickOnElement(payment, paymentMethod + " button");
        Log.info("Payed with"+ paymentMethod);
    }
}
