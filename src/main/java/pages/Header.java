package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import services.CustomService;

public class Header {
    private final WebDriver driver;
    private CustomService service;

    public Header(WebDriver driver){
        this.driver = driver;
        service = new CustomService(driver);
    }

    @FindBy(id = "menu-favorites")
    private WebElement heart;

    public void waitForHeartEnable(){
        service.waitClickable(heart);
    }
}
